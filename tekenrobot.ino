#include <cantino.h>
using namespace cantino;
#include <AccelStepper.h>

#include "rectangle.hpp"
#include "triangle.hpp"
#include "spiro.hpp"
#include "robot-controller.hpp"

//robot dimensions
float wheelSizeInMm = 67.90;
float axelSizeInMm = 127.0; //128.0
byte penServoPin = 11;
byte leftStepperPinIn1 = 2;
byte leftStepperPinIn2 = 3;
byte leftStepperPinIn3 = 4;
byte leftStepperPinIn4 = 5;
byte rightStepperPinIn1 = 6;
byte rightStepperPinIn2 = 7;
byte rightStepperPinIn3 = 8;
byte rightStepperPinIn4 = 9;

char input;
char amount;

int main()
{
    RobotController robot(
        wheelSizeInMm, axelSizeInMm,
        penServoPin,
        leftStepperPinIn1, leftStepperPinIn2, leftStepperPinIn3, leftStepperPinIn4,
        rightStepperPinIn1, rightStepperPinIn2, rightStepperPinIn3, rightStepperPinIn4);

        cout << "Wat wilt u tekenen? (Voer getal in)" << endl << "1. Vierkant" << endl
        << "2. Gelijkhoekige driehoek" << endl << "3. Spirogram" << endl << "4. Een van elk vorm" << endl;
        cin >> input;
        cout << "input>" << input;

        switch (input){
            case '4':
                cout << "Case 4..." << endl;
                Shape *shapeArray[3];
                shapeArray[0] = new Rectangle (100,100);
                shapeArray[1] = new Triangle (100,100);
                shapeArray[2] = new Spiro (60, 50);
                cout << "Een van elk vorm worden nu getekend..." << endl;

                for (int i = 0; i < 3; i++){
                    shapeArray[i]->draw(&robot);
                }
                break;

            case '1':
                cout << "Hoeveel vierkanten wilt u tekenen? (Max. 4)" << endl;
                cin >> amount;
                //cout << "input>" << amount << endl;
                amount = amount - '0';
                //cout << "amount int>" << (int)amount << endl;
                if (amount > 0 && amount <= 4){
                    cout << (int)amount << " vierkanten worden getekend..." << endl;
                    Rectangle rect1(100,100);
                    for(int i = 0; i < amount; i++){
                        cout << "--------------- vierkant nr " << i << "------------------------" << endl;
                        rect1.draw(&robot);
                    }
                    break;
                }
                else {
                    cout << "Kies een getal tussen 1 en 4" << endl;
                }
                break;

            case '2':
                cout << "Hoeveel driehoeken wilt u tekenen? (Max. 4)" << endl;
                cin >> amount;
                amount = amount - '0';
                if (amount > 0 && amount <= 4){
                    cout << (int)amount << " driehoeken worden getekend..." << endl;
                    Triangle tri1(100,100);
                    for(int i = 0; i < amount; i++){
                        tri1.draw(&robot);
                    }
                    break;
                }
                else {
                    cout << "Kies een getal tussen 1 en 4" << endl;
                }
                break;

            case '3':
                Spiro spiro1(60,50);
                cout << "Een spriogram wordt nu getekend" << endl;
                spiro1.draw(&robot);
                break;
            /*
            For some reason the switch case won't after the case '3'. After extensive debugging,
            it's unclear if this is cause by the case '3' statement above or the default statement below.
            */
            default:
                cout << "Geen geldige invoer. Invoer was " << input;
                break;
            }

        cout << endl << "------End of main-----" << endl << endl;

        return 0;
    }
