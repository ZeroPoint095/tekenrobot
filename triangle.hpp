#pragma once
#include "shape.hpp"

class Triangle : public Shape{
    public:
       int base;
       int height;

      Triangle(int const *x, int const *y);
      virtual ~Triangle();
      //float calcSides();
      //float calcAngleAlpha();
      void draw(RobotController *robot);
};
