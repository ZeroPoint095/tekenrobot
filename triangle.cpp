#include "triangle.hpp"
#include <cantino.h>
using namespace cantino;

Triangle::Triangle(const int *x, const int *y):Shape(x,y){

   base = x;
   height = y;
  cout << "Constructing triangle: " << width << "x" << height << endl;
}

Triangle::~Triangle(){
  cout << "Destroying triangle." << endl;
};

void Triangle::draw(RobotController *robot){
  cout << "Drawing " << width << " by " << height << " triangle." << endl;
  robot -> drawWithPen(1);
  for(int i = 0; i < 3; i++){
      robot -> forward(base);
      robot -> turnRight(120);
  }
  robot -> drawWithPen(0);
  cout << "Moving to new origin..." << endl;
  robot -> turnRight(45);
  robot -> forward(20);
  robot -> turnLeft(45);
};




// float Triangle::calcSides(){
//     return (base*height)/2;
// }
//
// float Triangle::calcAngleAlpha(){
//     float a = height;
//     float b = base/2;
//
//     degrees(tan(a/b));
// }
// /*      alpha
//               / |\
//              /  | \
//             /  a|  \
//            /    |   \
//            ----------
//               b
//            base /2
// */
// void Triangle::draw(RobotController *robot){
//     float theta = calcAngleTheta();
//     float sides = calcSides();
//   cout << "Drawing " << width << " by " << height << " triangle." << endl;
//   robot -> drawWithPen(0);
//   robot -> forward(height);
//   robot -> turnRight(180-theta));
//   robot -> forward(sides);
//   //this is where the robot control will come
// };
