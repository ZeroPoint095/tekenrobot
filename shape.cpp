#include "shape.hpp"

//constructor
Shape::Shape(const int *w, const int *h){
    width = &w;
    height = &h;
}

//destructor (virtual)
 Shape::~Shape(){
 }

//functions
//pure virtual, so doesn't need to be here
//void Shape::draw()=0;
