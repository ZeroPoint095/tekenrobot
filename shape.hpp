#pragma once
#include "robot-controller.hpp"
class Shape {
  public:
    int width, height;

    Shape(const int *w, const int *h);
    virtual ~Shape()=0;
    virtual void draw (RobotController *robot) =0;
};
