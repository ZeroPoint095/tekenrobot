#pragma once
#include "shape.hpp"
#include "robot-controller.hpp"
class Rectangle : public Shape{
    public:
       int width;
       int height;

      Rectangle(int const *x, int const *y);
      virtual ~Rectangle();
      void draw(RobotController *robot);
};
