#define debug 1

#include "robot-controller.hpp"
#include <cantino.h>
using namespace cantino;

static const int RobotController::halfstep = 4;

//static const int steps_rev=4096;      //   512 for 64x gearbox, 128 for 16x gearbox
static const int steps_rev=2048;      //   512 for 64x gearbox, 128 for 16x gearbox


static float maxSpeed = 500.0; //3000
static float acceleration = 800.0; //500
static float speed = 90.0; //300

float wheelSize;
float axelSize;

RobotController::RobotController(const float wheelSizeInMm, const float axelSizeInMm,
    const byte   penServoPin,
    const byte   leftStepperPinIn1, const byte   leftStepperPinIn2,
    const byte   leftStepperPinIn3, const byte   leftStepperPinIn4,
    const byte   rightStepperPinIn1, const byte   rightStepperPinIn2,
    const byte   rightStepperPinIn3, const byte   rightStepperPinIn4):
    wheelSize(wheelSizeInMm), axelSize(axelSizeInMm),penServoPin(penServoPin),
    stepperLeft(halfstep, leftStepperPinIn1, leftStepperPinIn3, leftStepperPinIn2, leftStepperPinIn4), //Order is IN1, IN3, IN2, IN4 //leftStepperPinIn3, 2,  werkt niet
    stepperRight(halfstep, rightStepperPinIn1, rightStepperPinIn3, rightStepperPinIn2, rightStepperPinIn4),
    penServo()
    {
        if (debug){
            cout << "Pins" << endl << penServoPin << endl << leftStepperPinIn1 << endl <<
            leftStepperPinIn2 << endl << leftStepperPinIn3 << endl << leftStepperPinIn4 << endl;
        }
        penServo.attach(penServoPin);
        penServo.write(40);

        wheelSize = wheelSizeInMm;
        axelSize = axelSizeInMm;

        setupRoutine(); //TODO: ipv dit, afgeleide klasse van stepper en servo maken en in constructor de pin in/out setten
    }

    RobotController::~RobotController(){
        cout << "Destroying RobotController." << endl;
    };

    void RobotController::setupRoutine(){
        //hier had ik pinMode() dingen staan, maar de libraries doen dit al (gecheckt)
        stepperLeft.setMaxSpeed(maxSpeed);
        stepperLeft.setAcceleration(acceleration);
        stepperLeft.setSpeed(speed);

        stepperRight.setMaxSpeed(maxSpeed);
        stepperRight.setAcceleration(acceleration);
        stepperRight.setSpeed(speed);
    };

    void RobotController::drawWithPen(bool penDown){
        if (penDown){
            penServo.write (150);
        }
        else {
            penServo.write(40);
        }
        cout << "Pen up: " << penDown << endl;
    };

    int RobotController::steps(int distance){
        float steps = distance * (steps_rev / (wheelSize * 3.1412));
        //( distance * steps_rev) / (wheelSize * 3.1412);
        cout << "Distance:" << distance << " Steps: " << steps << endl;
        return int(steps);
    }

    void RobotController::forward(int distance){
        cout << "Moving forward " << distance << "mm." << endl;

        stepperLeft.move(-(steps(distance)));
        stepperRight.move(-(steps(distance)));

        while(stepperLeft.distanceToGo() != 0 && stepperRight.distanceToGo() != 0)
        {
            if (stepperLeft.distanceToGo() > stepperRight.distanceToGo()){
                stepperLeft.run();
            }
            if (stepperLeft.distanceToGo() < stepperRight.distanceToGo()){
                stepperRight.run();
            }
            stepperLeft.run();
            stepperRight.run();
        }
    };

    void RobotController::back(int distance){
        cout << "Moving back " << distance << "mm." << endl;
        stepperLeft.move(distance);
        stepperRight.move(distance);
        while(stepperLeft.distanceToGo() != 0 && stepperRight.distanceToGo() != 0)
        {
            stepperLeft.run();
            stepperRight.run();
        }
    };

    void RobotController::turnLeft(float angle){
        cout << "Turning left " << angle << " degrees." << endl;
        float rotation = angle / 360.0;
        float distance = axelSize * 3.1412 * rotation;
        int step = steps(distance);
        stepperLeft.move(step);
        stepperRight.move(-step);

        while(stepperLeft.distanceToGo() != 0 && stepperRight.distanceToGo() != 0)
        {
            stepperLeft.run();
            stepperRight.run();
        }
    };

    void RobotController::turnRight(float angle){
        cout << "Turning right " << angle << " degrees." << endl;
        float rotation = angle / 360.0;
        float distance = axelSize * 3.1412 * rotation;
        int step = steps(distance);
        stepperLeft.move(-step);
        stepperRight.move(step);
        while(stepperLeft.distanceToGo() != 0 && stepperRight.distanceToGo() != 0)
        {
            stepperLeft.run();
            stepperRight.run();
        }
    }

    // float getNearestAngle(float angle_){
    //     // code contributed by Instructable user PMPU_student
    //     /*
    //     https://github.com/aspro648/OSTR/blob/master/V2/firmware/Arduino%2032U4/Itsy_calibration_lib/Itsy_calibration_lib.ino
    //     Lets rotate by 58 degrees.
    //     turnRight(58); // rotate only by 57.631138392857146 degrees(that is the
    //     value that is the closest to the desired one _and_ is lesser than it).
    //     That means you are doing a certain amount of motor steps. But if you do
    //     one more step during rotation you will rotate by 58.0078125 degrees,
    //     which is much more precise(but it is greater than desired value, thats
    //     why in original code it is not picked). The thing is that original code
    //     always decides to rotate by value that is lesser than desired value while
    //     it is sometimes worse than choosing the value that is bigger.
    //     My code chooses from both variants, minimizing the error.
    //     */
    //     float angle_error = 0;
    //     float angle = 0;
    //     int step = 0;
    //     float previousAngle = 0;
    //     float step_length = 3.1412 * wheelSize / steps_rev;
    //     angle_ += angle_error;
    //     cout << "while?";
    //     while(!(previousAngle <= angle_ && angle_ <= angle)){
    //         step += 1;
    //         previousAngle = angle;
    //         angle = step * step_length * 360 / (axelSize * 3.1412) + 0.01;
    //     }
    //     float dif1 = angle_- angle;
    //     float dif2 = angle_- previousAngle;
    //     if(abs(dif1) < abs(dif2)){
    //         cout << "if";
    //         angle_error = dif1;
    //         Serial.println(angle_error);
    //         return angle;
    //     }else{
    //         cout << "else";
    //         angle_error = dif2;
    //         Serial.println(angle_error);
    //         return previousAngle;
    //     }
    // }
