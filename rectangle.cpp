#include "rectangle.hpp"
#include <cantino.h>
using namespace cantino;

Rectangle::Rectangle(const int *x, const int *y):Shape(x,y){

   width = x;
   height = y;
  cout << "Constructing rectangle: " << width << "x" << height << endl;
}

Rectangle::~Rectangle(){
  cout << "Destroying rectangle." << endl;
};

void Rectangle::draw(RobotController *robot){
  cout << "Drawing " << width << " by " << height << " rectangle." << endl;
  robot -> drawWithPen(1);
  for(int i =0; i < 4; i++){
      robot -> forward(height);     //http://www.cplusplus.com/forum/beginner/53293/
      robot -> turnRight(90);       //It's a combination of dereferencing and member access.
                                    //instead of foo->bar you can write (*foo).bar.

  }
  robot -> drawWithPen(0);
  cout << "Moving to new origin..." << endl;
  robot -> turnRight(45);
  robot -> forward(20);
  robot -> turnLeft(45);
};
