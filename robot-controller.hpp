#pragma once
#include <arduino.h> //needed for byte datatype
#include <Servo.h>
#include <AccelStepper.h>

class RobotController {

  public:
    RobotController(const float   wheelSizeInMm, const float axelSizeInMm,
                    const byte   penServoPin,
                    const byte   leftStepperPinIn1, const byte   leftStepperPinIn2,
                    const byte   leftStepperPinIn3, const byte   leftStepperPinIn4,
                    const byte   rightStepperPinIn1, const byte   rightStepperPinIn2,
                    const byte   rightStepperPinIn3, const byte   rightStepperPinIn4);



    virtual ~RobotController();

    void setupRoutine();
    int steps(int distance);
    void forward(int distance);
    void back(int distance);
    void turnLeft(float angle);
    void turnRight(float angle);
    void drawWithPen(bool penDown);

    float wheelSize;
    float axelSize;

  private:
    static const int halfstep;

    byte penServoPin;

    byte leftStepperPinIn1;
    byte leftStepperPinIn2;
    byte leftStepperPinIn3;
    byte leftStepperPinIn4;
    byte rightStepperPinIn1;
    byte rightStepperPinIn2;
    byte rightStepperPinIn3;
    byte rightStepperPinIn4;
    AccelStepper stepperLeft;
    AccelStepper stepperRight;
    Servo penServo;
};
